//John Karasev Copyright 2018

use reqwest;
use reqwest::Result;
use serde_json::{Value, Error, json};
use super::{Demands, DemandForecasts};

pub fn get_all_forecasts() -> Result<Value> {
    let authorities = DemandForecasts::new();
    let client = reqwest::Client::new();

    let mut megawatts: Vec<i64> = Vec::new();

    for authority in authorities.bal_auth {
        let mut res = client
            .get(&format!("http://api.eia.gov/series/?api_key=81e8e7f69cce299bf1437f0feb2ae191&series_id={}", authority.id))
            .send()?
            .json()?;
        println!("HELLOOOOOO {}", serde_json::to_string_pretty(&res).unwrap());
    }

    Ok(json!({"status": "ok"}))


}

