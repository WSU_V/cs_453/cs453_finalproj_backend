//John Karasev Copyright 2018

#![feature(proc_macro_hygiene, decl_macro)]


#[macro_use(bson, doc)]
extern crate bson;
extern crate mongodb;

extern crate dotenv;

#[macro_use] extern crate rocket;
//extern crate rocket_contrib;
//extern crate rocket_cors;

extern crate chrono;

extern crate frank_jwt as jwt;
extern crate crypto;

extern crate ws;
extern crate reqwest;

extern crate r2d2;
extern crate r2d2_mongodb;

#[macro_use] extern crate serde_derive;
#[macro_use] extern crate serde_json;

use std::sync::Mutex;

use serde_json::Value;

use rocket::{Rocket, State};
use rocket_contrib::json::{Json, JsonValue};

use mongodb::{Client};
use mongodb::coll::Collection;

use r2d2::Pool;
use r2d2_mongodb::{ConnectionOptionsBuilder, MongodbConnectionManager};


pub mod db;
mod routes;
mod config;
mod oauth;
mod fetch;
mod cors;


fn main() {

    // intialize the mongodb connections.
    let pool = db::init_pool();

    //create an admin user.
    {
        match pool.get() {
            Ok(dbs) => {
                let conn = db::Conn(dbs);
                let _: Result<(), mongodb::Error> = match db::create_admin(&conn) {
                    Ok(res) => Ok(()),
                    Err(mongodb::Error::DefaultError(s)) => {
                        if s == "email already taken" {
                            Ok(())
                        } else {
                            panic!("cannot create admin user");
                        }
                    },
                    Err(e) => panic!("cannot create admin user"),
                };
            },
            Err(e) => panic!("failed to get a mongodb connection from connection pool")
        }
    }

    // launch the app and mount all the api endpoints.
    rocket::ignite()
        .manage(pool)
        .register(catchers![not_found])
        //.attach(cors::CORS())
        .mount("/", routes![routes::loads::get_loads,
                            routes::loads::one_load,
                            routes::loads::get_comparisons,
                            routes::loads::get_forecasts,
                            routes::users::login_user,
                            routes::users::post_create_user,
                            routes::users::get_user_user,
                            routes::users::get_user_admin,
                            routes::users::put_user_update,
                            routes::users::put_manager_user_update,
                            routes::users::put_admin_user_update,
                            routes::users::delete_manager_user,
                            routes::users::delete_admin_user])
        .launch();
}


//if no endpoints matched, return error.
#[catch(404)]
fn not_found() -> Json<Value> {
    Json(json!({
        "status": "fail",
        "reason": "Resource was not found."
    }))
}


