// John Karasev Copyright 2018
pub const DATE_FORMAT: &'static str = "%Y-%m-%dT%H:%M:%S";
//'2018-10-01T00:00:00'
pub const RESPONSE_FORMAT: &'static str = "%Y-%m-%dT%H:%M:%S";
pub const TOKEN_PREFIX: &'static str = "Bearer ";
pub const SECRET: &'static str = "secret123";

