//John Karasev Copyright 2018

/*
    A list of api endpoints related to retrieving power load data.
*/

use super::super::db::loads::*;

use chrono::prelude::*;

use super::super::config;

use mongodb::db::ThreadedDatabase;

use rocket::State;
use rocket_contrib::json::Json;

use serde_json::Value;

use rocket::request::{Form, LenientForm};

use super::super::db::Conn;

use bson::Bson::{UtcDatetime, I64};

use super::guards;

use super::super::oauth;

#[derive(FromForm)]
pub struct LoadRange {
    pub start: String,
    pub end: String,
}

//implement extract start and end date from the range object
impl LoadRange {
    pub fn utc_start(&self) -> DateTime<Utc> {
        Utc.datetime_from_str(&self.start, config::DATE_FORMAT)
            .unwrap()
    }
    pub fn utc_end(&self) -> DateTime<Utc> {
        Utc.datetime_from_str(&self.end, config::DATE_FORMAT)
            .unwrap()
    }
}

/* Serialize and Deserialize objects that are being communicated. */
#[derive(FromForm, Deserialize, Serialize)]
pub struct LoadEntry {
    pub time: String,
    pub load: i64,
}

#[derive(Deserialize, Serialize)]
pub struct LoadResponse {
    pub data: Vec<LoadEntry>,
}

/* This enpoint accepts a single load and updates the database*/
#[post("/loads/one", format = "application/json", data = "<entry>")]
pub fn one_load(entry: Json<LoadEntry>, conn: Conn) -> Json<Value> {
    let data = entry.into_inner();
    let date = match Utc.datetime_from_str(&data.time, config::DATE_FORMAT) {
        Ok(d) => d,
        Err(e) => return Json(json!({"status": "fail"})),
    };
    match insert_one_load(date, data.load, &conn) {
        Ok(t) => Json(json!({"status": date.to_string()})),
        Err(e) => Json(json!({"status": "something went wrong :)"})),
    }
}

/* returns date, forecast, actual, and std error for a date range. */
#[get("/forecasts/comparisons?<range..>")]
pub fn get_comparisons(range: Form<LoadRange>, any_user: &oauth::Auth, conn: Conn) -> Json<Value> {
    let start = range.utc_start();
    let end = range.utc_end();

    let vrange = range.into_inner();

    let load_docs = match get_loads_from_range(&vrange, "loads", &conn) {
        Ok(res) => res,
        Err(e) => return Json(json!({"status": "fail", "description": "mongo error"})),
    };

    let forecast_docs = match get_loads_from_range(&vrange, "forecasts", &conn) {
        Ok(res) => res,
        Err(e) => return Json(json!({"status": "fail", "description": "mongo error"})),
    };

    let mut response: Vec<Value> = Vec::new();
    for (l, f) in load_docs.zip(forecast_docs) {
        let mut hour: Vec<Value> = Vec::new();
        let load = l.unwrap();
        let lv = serde_json::to_value(load.clone()).unwrap();
        let fv = serde_json::to_value(f.unwrap()).unwrap();
        hour.push(Value::String(
            load.get_utc_datetime("time")
                .unwrap()
                .format(config::RESPONSE_FORMAT)
                .to_string(),
        ));
        hour.push(lv["load"].clone());
        hour.push(fv["load"].clone());
        hour.push(fv["percent_error"].clone());
        response.push(Value::Array(hour));
    }

    Json(Value::Array(response))
}

/* returns time and actual load */
#[get("/loads?<range..>")]
pub fn get_loads(range: Form<LoadRange>, any_user: &oauth::Auth, conn: Conn) -> Json<Value> {
    let start = range.utc_start();
    let end = range.utc_end();

    let vrange = range.into_inner();

    let load_docs = match get_loads_from_range(&vrange, "loads", &conn) {
        Ok(res) => res,
        Err(e) => return Json(json!({"status": "fail", "description": "mongo error"})),
    };

    let mut response: Vec<Value> = Vec::new();
    for l in load_docs {
        let mut hour: Vec<Value> = Vec::new();
        let load = l.unwrap();
        let lv = serde_json::to_value(load.clone()).unwrap();
        hour.push(Value::String(
            load.get_utc_datetime("time")
                .unwrap()
                .format(config::RESPONSE_FORMAT)
                .to_string(),
        ));
        hour.push(lv["load"].clone());
        response.push(Value::Array(hour));
    }

    Json(Value::Array(response))
}

/*returns time, forecast load, and stderr*/
#[get("/forecasts?<range..>")]
pub fn get_forecasts(range: Form<LoadRange>, any_user: &oauth::Auth, conn: Conn) -> Json<Value> {
    let start = range.utc_start();
    let end = range.utc_end();

    let vrange = range.into_inner();

    let forecast = match get_loads_from_range(&vrange, "forecasts", &conn) {
        Ok(res) => res,
        Err(e) => return Json(json!({"status": "fail", "description": "mongo error"})),
    };

    let mut response: Vec<Value> = Vec::new();
    for f in forecast {
        let mut hour: Vec<Value> = Vec::new();
        let doc = f.unwrap();
        let fv = serde_json::to_value(doc.clone()).unwrap();
        hour.push(Value::String(
            doc.get_utc_datetime("time")
                .unwrap()
                .format(config::RESPONSE_FORMAT)
                .to_string(),
        ));
        hour.push(fv["load"].clone());
        hour.push(fv["percent_error"].clone());
        response.push(Value::Array(hour));
    }

    Json(Value::Array(response))
}
