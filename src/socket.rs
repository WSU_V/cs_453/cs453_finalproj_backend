//John Karasev Copyright 2018
extern crate ws;


fn start_progress_socket() {
    listen("127.0.0.1:3012", |out| {
        move |msg| {
            println!("PROGRESS SERVER {}", msg);
            out.send(msg)
        }
    }).unwrap()
}

fn start_forecast_socket() {
    listen("127.0.0.1:3013", |out| {
        move |msg| {
            println!("FORECAST SERVER {}", msg);
            out.send(msg)
        }
    }).unwrap()
}

