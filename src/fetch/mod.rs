//John Karasev Copyright 2018

pub mod fetch_forecast;

/*
#[derive(Serialize, Deserialize)]
pub struct Data {
    pub data: Vec<DataPoint>
}

#[derive(Serialize, Deserialize)]
pub type DataPoint = Vec<String>;
*/

pub struct BalAuth<'a> {
    id: &'a str,
    weight: f64
}

const BPA_DEMAND: &'static str = "EBA.BPAT-ALL.D.H";
const BPA_DEMAND_FORECAST: &'static str = "EBA.BPAT-ALL.DF.H";


struct Demands<'a> {
    bal_auth: Vec<BalAuth<'a>>
}

impl Demands<'static> {
    pub fn new() -> Demands<'static> {
        Demands {
            bal_auth: vec![BalAuth { id: BPA_DEMAND, weight: 1.0}]
        }
    }
}

struct DemandForecasts<'a> {
    bal_auth: Vec<BalAuth<'a>>
}

impl DemandForecasts<'static> {
    pub fn new() -> DemandForecasts<'static> {
        DemandForecasts {
            bal_auth: vec![BalAuth { id: BPA_DEMAND_FORECAST, weight: 1.0}]
        }
    }
}