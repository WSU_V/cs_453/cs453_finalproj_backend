//John Karasev Copyright 2018

/*
    Provides the general functions for database
*/

use dotenv::dotenv;
use std::env;

use mongodb::coll::{results, Collection};
use mongodb::db::{Database, ThreadedDatabase};
use mongodb::{Client, ThreadedClient};

use r2d2::{ManageConnection, Pool, PooledConnection};
use r2d2_mongodb::{ConnectionOptionsBuilder, MongodbConnectionManager};

use rocket::http::Status;
use rocket::request::{self, FromRequest};
use rocket::{Outcome, Request, State};

use std::ops::Deref;

pub mod loads;
pub mod users;

/*
    Wrap the pooled connection into a Conn struct.
*/
pub struct Conn(pub PooledConnection<MongodbConnectionManager>);

/*
    create a connection pool of mongodb connections to allow a lot of users to modify db at same time.
*/
pub fn init_pool() -> Pool<MongodbConnectionManager> {
    dotenv().ok();
    let mongo_addr = env::var("MONGO_ADDR").expect("MONGO_ADDR must be set");
    let mongo_port = env::var("MONGO_PORT").expect("MONGO_PORT must be set");
    let db_name = env::var("DB_NAME").expect("DB_NAME env var must be set");
    let manager = MongodbConnectionManager::new(
        ConnectionOptionsBuilder::new()
            .with_host(&mongo_addr)
            .with_port(mongo_port.parse::<u16>().unwrap())
            .with_db(&db_name)
            .build(),
    );
    match Pool::builder().max_size(64).build(manager) {
        Ok(pool) => pool,
        Err(e) => panic!("Error: failed to create mongodb pool {}", e),
    }
}

/* At start create a admin user */
pub fn create_admin(db: &Database) -> Result<results::InsertOneResult, mongodb::Error> {
    users::create_user("admin@gmail.com".to_string(), "admin1".to_string(), 1, db)
}

/*
    When Conn is dereferencd, return the mongo connection.
*/
impl Deref for Conn {
    type Target = <r2d2_mongodb::MongodbConnectionManager as ManageConnection>::Connection;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/*
    Create a implementation of FromRequest so Conn can be provided at every api endpoint
*/
impl<'a, 'r> FromRequest<'a, 'r> for Conn {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Conn, ()> {
        let pool = request.guard::<State<Pool<MongodbConnectionManager>>>()?;
        match pool.get() {
            Ok(db) => Outcome::Success(Conn(db)),
            Err(_) => Outcome::Failure((Status::ServiceUnavailable, ())),
        }
    }
}
