//John Karasev Copyright 2018

/*
    This is the main function that is ran when the database is seeded. cargo run --bin seed
*/


#[macro_use(bson, doc)]
extern crate bson;
extern crate mongodb;

extern crate dotenv;

extern crate chrono;

extern crate rand;

extern crate reqwest;
extern crate ws;

extern crate r2d2;
extern crate r2d2_mongodb;

#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;

use r2d2::Pool;
use r2d2_mongodb::{ConnectionOptionsBuilder, MongodbConnectionManager};

use serde_json::{json, Error, Value};
use std::io;
use std::fs::{self, DirEntry, File};
use std::path::Path;
use std::env;


use mongodb::coll::{results, Collection};
use mongodb::db::{Database, ThreadedDatabase};
use mongodb::{Client, ThreadedClient};

use bson::Bson::{UtcDatetime, I64, FloatingPoint};
use bson::Document;

use chrono::prelude::*;

use rand::prelude::*;

use mongodb::error;

fn main() {
    let path = env::current_dir().unwrap();
    let client = Client::connect("localhost", 27017).expect("failed to connect to mongodb"); //connect
    let _: error::Result<()> = client.drop_database("forecast"); // drop old
    let dbs = client.db("forecast");  // create a new database.
    dbs.create_collection("forecasts", None).expect("fail to create forecast");
    dbs.create_collection("loads", None).expect("fail to create loads");
    dbs.create_collection("users", None).expect("fail to create users");
    /* load in the electric power data. */
    load(
        "./data/demand_forecast.json",
        "forecasts",
        &dbs,
        true
    );
    load(
        "./data/demand.json",
        "loads",
        &dbs,
        false
    );

}

/*reads the json object from file*/
pub fn get_all(path: &str) -> Value {
    let path = Path::new(path);
    let file = File::open(path).expect("file not found");
    let val: Value = serde_json::from_reader(file).unwrap();
    val
}

/* loads the data from files into mongodb. */
pub fn load(path: &str, coll: &str, db: &Database, is_forecast: bool) {
    let mut contents: Vec<Value> = match get_all(path)["data"].clone() {
        Value::Array(v) => v,
        _ => panic!("not an array"),
    };

    for mut point in &contents {
        let mut rng = thread_rng();
        let mut doc = Document::new();
        match point[0].clone() {
            Value::String(mut s) => {
                s.pop();
                s.push_str(":00:00Z");
                let date = Utc.datetime_from_str(&s, "%Y%m%dT%H:%M:%SZ").ok().expect("failed to parse date time");
                doc.insert("time", UtcDatetime(date));
            }
            _ => panic!("incorrect format"),
        }
        match point[1].clone() {
            Value::Number(num) => {
                if let Some(int64) = num.as_i64() {
                    doc.insert("load", I64(int64));
                    if is_forecast {
                        let percent_err = rng.gen::<f64>() * 0.25;
                        let load_err = percent_err * (int64 as f64);
                        doc.insert("percent_error", percent_err);
                        doc.insert("load_error", load_err);
                    }
                } else {
                    panic!("cannot convert into int64");
                }
            },
            Value::Null => {
                doc.insert("load", I64(0));
                if is_forecast {
                    doc.insert("percent_error", FloatingPoint(0.0));
                    doc.insert("load_error", FloatingPoint(0.0));
                }
            },
            _ => panic!("did not match a number")
        }

        db.collection(coll).insert_one(doc, None).unwrap();
    }
}



