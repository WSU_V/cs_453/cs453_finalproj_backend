//John Karasev Copyright 2018

use dotenv::dotenv;
use std::env;

use super::super::routes::loads::LoadRange;

use super::Conn;

use mongodb::coll::results::InsertOneResult;
use mongodb::coll::Collection;
use mongodb::cursor::Cursor;
use mongodb::db::{Database, ThreadedDatabase};
use mongodb::Client;

//use bson::ordered::OrderedDocument;
use bson::Bson::UtcDatetime;
use bson::{decode_document, encode_document, Bson, Document};
use chrono::prelude::*;

use super::super::config;

/* given a date range, retrieves a range of loads from a collection in the db. */
pub fn get_loads_from_range(
    date_range: &LoadRange,
    coll: &str,
    db: &Database,
) -> Result<Cursor, mongodb::Error> {

    let start = date_range.utc_start();
    let end = date_range.utc_end();

    let doc = doc! {
        "time" : {"$gte": bson::Bson::UtcDatetime(start), "$lte": bson::Bson::UtcDatetime(end)}
    };
    db.collection(coll).find(Some(doc), None)
}

/* inserts a single load into the mongodb database */
pub fn insert_one_load(
    date: DateTime<Utc>,
    load: i64,
    db: &Database,
) -> Result<InsertOneResult, mongodb::Error> {
    let mut doc = Document::new();
    doc.insert("time", bson::Bson::UtcDatetime(date.clone()));
    doc.insert("load", bson::Bson::I64(load));
    db.collection("loads").insert_one(doc.clone(), None)
}
