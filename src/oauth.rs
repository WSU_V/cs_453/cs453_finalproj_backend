//John Karasev Copyright 2018
use rocket::request::{self, FromRequest, Request};
use rocket::Outcome;

use jwt;
use serde_json;

use super::config;

#[derive(Debug, Deserialize, Serialize)]
pub struct Auth {
    /// timestamp
    pub exp: i64,
    // email
    pub email: String,
    // admin = 1, manager = 2, or user = 3
    pub user_type: i32
}

impl Auth {
    pub fn token(&self) -> String {
        let header = json!({});
        let payload = json!(self);
        jwt::encode(
            header,
            &config::SECRET.to_string(),
            &payload,
            jwt::Algorithm::HS256,
        )
            .expect("jwt")
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for &'a Auth {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<&'a Auth, ()> {
        let res = request.local_cache(|| {
            extract_auth_from_request(request)
        });

        match res {
            Some(auth) => Outcome::Success(auth),
            None => Outcome::Forward(())
        }
    }
}

fn extract_auth_from_request(request: &Request) -> Option<Auth> {
    let header = request.headers().get("Token").next();
    if let Some(token) = header.and_then(extract_token_from_header) {
        match jwt::decode(
            &token.to_string(),
            &config::SECRET.to_string(),
            jwt::Algorithm::HS256,
        ) {
            Err(err) => {
                println!("Auth decode error: {:?}", err);
            }
            Ok((_, payload)) => match serde_json::from_value::<Auth>(payload) {
                Ok(auth) => return Some(auth),
                Err(err) => println!("Auth serde decode error: {:?}", err),
            },
        };
    }
    None
}

fn extract_token_from_header(header: &str) -> Option<&str> {
    if header.starts_with(config::TOKEN_PREFIX) {
        Some(&header[config::TOKEN_PREFIX.len()..])
    } else {
        None
    }
}
