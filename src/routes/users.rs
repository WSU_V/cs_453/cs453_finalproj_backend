//John Karasev Copyright 2018

/*
    A list of api endpoints that are related to user operations.
*/
use rocket::request::{self, FromRequest, Request};
use rocket::Outcome;

use mongodb;

use super::super::db;

use super::super::config;

use super::super::oauth::Auth;

use super::guards;

use rocket::http::RawStr;
use rocket::request::{Form, LenientForm};
use rocket_contrib::json::{Json, JsonValue};
use serde_json::Value;
//use serde_json;

/*Deserialize the objects given at login. */
#[derive(Deserialize)]
pub struct LoginUser {
    pub user: LoginUserData,
}

#[derive(Deserialize)]
pub struct LoginUserData {
    pub email: String,
    pub password: String,
}

/* Logs a user in */
#[post("/oauth/login", format = "application/json", data = "<jcred>")]
pub fn login_user(jcred: Json<LoginUser>, conn: db::Conn) -> Json<Value> {
    let cred = jcred.into_inner();
    let doc = match db::users::login(cred.user.email, cred.user.password, &conn) {
        Ok(doc) => match doc {
            Some(d) => d,
            None => return Json(json!({"status": "fail", "description": "login failure"})),
        },
        Err(mongodb::Error::DefaultError(s)) => {
            return Json(json!({"status": "fail", "description": s}))
        }
        Err(e) => return Json(json!({"status": "fail", "description": "internal error"})),
    };

    let email = doc.get("email").unwrap().as_str().unwrap();
    let user_type = doc.get("user_type").unwrap().as_i32().unwrap();

    let token = Auth {
        exp: (chrono::Utc::now() + chrono::Duration::days(60)).timestamp(),
        email: email.to_string(),
        user_type,
    }
    .token();

    Json(json!({ "Token": token }))
}

#[derive(Deserialize)]
pub struct NewUser {
    user: NewUserData,
}

#[derive(Deserialize)]
pub struct NewUserData {
    email: String,
    password: String,
    user_type: i32,
}

/* endpoint for creating a new user */
#[post("/users/create", format = "application/json", data = "<jnew>")]
pub fn post_create_user(
    jnew: Json<NewUser>,
    admin: guards::AdminManager,
    conn: db::Conn,
) -> Json<Value> {
    let new = jnew.into_inner();
    if new.user.user_type == 1 {
        return Json(json!({"status": "fail", "description": "can only create one admin"}));
    }
    match db::users::create_user(new.user.email, new.user.password, new.user.user_type, &conn) {
        Ok(t) => Json(json!({"status": "ok"})),
        Err(e) => match e {
            mongodb::Error::DefaultError(desc) => {
                Json(json!({"status": "fail", "description": desc}))
            }
            _ => Json(json!({"status": "fail", "description": "failed to create user"})),
        },
    }
}

#[derive(Serialize, Deserialize)]
struct UserInfo {
    email: String,
    user_type: i32,
}

/* returns email and user type given an email */
#[get("/users?<email>")]
pub fn get_user_admin(email: &RawStr, admin: guards::AdminManager, conn: db::Conn) -> Json<Value> {
    let doc = match db::users::user_info(email.as_str().to_string(), &conn) {
        Ok(res) => match res {
            Some(doc) => doc,
            None => return Json(json!({"status": "fail", "description": "user not found"})),
        },
        Err(mongodb::Error::DefaultError(s)) => {
            return Json(json!({"status": "fail", "description": s}))
        }
        Err(e) => return Json(json!({"status": "fail", "description": "mongo error"})),
    };
    let info = UserInfo {
        email: match doc.get_str("email") {
            Ok(s) => s.to_string(),
            Err(e) => "".to_string(),
        },
        user_type: match doc.get_i32("user_type") {
            Ok(s) => s,
            Err(e) => 0, //this should never happen...
        },
    };
    Json(serde_json::to_value(info).unwrap())
}

/* returns user info with the email provided in the JWT */
#[get("/users")]
pub fn get_user_user(user: &Auth, conn: db::Conn) -> Json<Value> {
    let doc = match db::users::user_info(user.email.clone(), &conn) {
        Ok(res) => match res {
            Some(doc) => doc,
            None => return Json(json!({"status": "fail", "description": "user not found"})),
        },
        Err(mongodb::Error::DefaultError(s)) => {
            return Json(json!({"status": "fail", "description": s}))
        }
        Err(s) => return Json(json!({"status": "fail", "description": "mongo error"})),
    };
    let info = UserInfo {
        email: match doc.get_str("email") {
            Ok(s) => s.to_string(),
            Err(e) => "".to_string(),
        },
        user_type: match doc.get_i32("user_type") {
            Ok(s) => s,
            Err(e) => 0, //this should never happen...
        },
    };
    Json(serde_json::to_value(info).unwrap())
}

#[derive(Deserialize)]
pub struct UpdateUser {
    email: String,
    update: UpdateUserData,
}

#[derive(Deserialize)]
pub struct UpdateUserData {
    email: Option<String>,
    password: Option<String>,
    user_type: Option<i32>,
}

//Updates user info when a user makes the request
#[put("/users/update", format = "application/json", data = "<jupdate>")]
pub fn put_user_update(
    jupdate: Json<UpdateUser>,
    user: guards::User,
    conn: db::Conn,
) -> Json<Value> {
    let update = jupdate.into_inner();

    if update.email != user.0.email {
        return Json(json!({"status": "fail", "description": "not privileged enough :("}));
    }
    if let Some(user_type) = update.update.user_type {
        return Json(json!({"status": "fail", "description": "not privileged enough :("}));
    }

    match db::users::update_user(
        update.email,
        update.update.email,
        update.update.password,
        update.update.user_type,
        &conn,
    ) {
        Ok(res) => Json(json!({"status": "ok"})),
        Err(mongodb::Error::DefaultError(s)) => Json(json!({"status": "fail", "description": s})),
        Err(e) => Json(json!({"status": "fail", "description": "mongo error"})),
    }
}


// update user info when the manager makes the request
#[put(
    "/users/update",
    format = "application/json",
    data = "<jupdate>",
    rank = 2
)]
pub fn put_manager_user_update(
    jupdate: Json<UpdateUser>,
    manager: guards::Manager,
    conn: db::Conn,
) -> Json<Value> {
    let update = jupdate.into_inner();

    if let Some(user_type) = update.update.user_type {
        if user_type == 1 {
            return Json(json!({"status": "fail", "description": "managers cannot set to admin"}));
        }
    }

    if let Some(user_type) = db::users::get_user_type(update.email.clone(), &conn) {
        if user_type < 3 {
            return Json(
                json!({"status": "fail", "description": "managers cannot update other managers or the admin"}),
            );
        }
    }

    match db::users::update_user(
        update.email,
        update.update.email,
        update.update.password,
        update.update.user_type,
        &conn,
    ) {
        Ok(res) => Json(json!({"status": "ok"})),
        Err(mongodb::Error::DefaultError(s)) => Json(json!({"status": "fail", "description": s})),
        Err(e) => Json(json!({"status": "fail", "description": "mongo error"})),
    }
}


//update user info when the admin makes the request.
#[put(
    "/users/update",
    format = "application/json",
    data = "<jupdate>",
    rank = 3
)]
pub fn put_admin_user_update(
    jupdate: Json<UpdateUser>,
    admin: guards::Admin,
    conn: db::Conn,
) -> Json<Value> {
    let update = jupdate.into_inner();

    if let Some(user_type) = db::users::get_user_type(update.email.clone(), &conn) {
        if user_type == 1 {
            if let Some(new_user_type) = update.update.user_type {
                return Json(
                    json!({"status": "fail", "description": "cannot update user_type for admin"}),
                );
            }
        }
    }

    if let Some(user_type) = update.update.user_type {
        if user_type == 1 {
            return Json(
                json!({"status": "fail", "description": "cannot make more than one admin"}),
            );
        }
    }

    match db::users::update_user(
        update.email,
        update.update.email,
        update.update.password,
        update.update.user_type,
        &conn,
    ) {
        Ok(res) => Json(json!({"status": "ok"})),
        Err(mongodb::Error::DefaultError(s)) => Json(json!({"status": "fail", "description": s})),
        Err(e) => Json(json!({"status": "fail", "description": "mongo error"})),
    }
}

//delete a user when the manager makes the request.
#[delete("/users/delete/<raw_email>")]
pub fn delete_manager_user(
    raw_email: &RawStr,
    manager: guards::Manager,
    conn: db::Conn,
) -> Json<Value> {
    let email = raw_email.to_string();
    if let Some(user_type) = db::users::get_user_type(email.clone(), &conn) {
        if user_type != 3 {
            return Json(
                json!({"status": "fail", "description": "manager cannot remove manager or admins"}),
            );
        }
    }
    match db::users::delete_user(email, &conn) {
        Ok(res) => Json(json!({"status": "ok"})),
        Err(e) => Json(json!({"status": "fail", "description": "mongodb error"})),
    }
}

//delete a user when an admin makes the request.
#[delete("/users/delete/<raw_email>", rank = 2)]
pub fn delete_admin_user(raw_email: &RawStr, admin: guards::Admin, conn: db::Conn) -> Json<Value> {
    let email = raw_email.to_string();
    if let Some(user_type) = db::users::get_user_type(email.clone(), &conn) {
        if user_type == 1 {
            return Json(json!({"status": "fail", "description": "admin cannot be deleted"}));
        }
    }
    match db::users::delete_user(email, &conn) {
        Ok(res) => Json(json!({"status": "ok"})),
        Err(e) => Json(json!({"status": "fail", "description": "mongodb error"})),
    }
}
