//John Karasev Copyright 2018

use rocket::request::{self, FromRequest, Request};
use rocket::Outcome;

use super::super::oauth::Auth;

/*
    A definition of guards that determines what type of user is accessing the backend.
*/


pub struct Admin<'a>(pub &'a Auth);
pub struct Manager<'a>(pub &'a Auth);
pub struct User<'a>(pub &'a Auth);
pub struct AdminManager<'a>(pub &'a Auth);

impl<'a, 'r> FromRequest<'a, 'r> for Admin<'a> {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Admin<'a>, ()> {
        let auth = request.guard::<&Auth>()?;
        if auth.user_type == 1 {
            // managers and admins can create users.
            Outcome::Success(Admin(auth))
        } else {
            Outcome::Forward(())
        }
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for Manager<'a> {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Manager<'a>, ()> {
        let auth = request.guard::<&Auth>()?;
        if auth.user_type == 2 {
            Outcome::Success(Manager(auth))
        } else {
            Outcome::Forward(())
        }
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for User<'a> {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<User<'a>, ()> {
        let auth = request.guard::<&Auth>()?;
        if auth.user_type == 3 {
            Outcome::Success(User(auth))
        } else {
            Outcome::Forward(())
        }
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for AdminManager<'a> {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<AdminManager<'a>, ()> {
        let auth = request.guard::<&Auth>()?;
        if auth.user_type < 3 {
            Outcome::Success(AdminManager(auth))
        } else {
            Outcome::Forward(())
        }
    }
}