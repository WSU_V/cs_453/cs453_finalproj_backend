//John Karasev Copyright 2018
/*
    Database functions specific to user operations.
*/

use std::sync::MutexGuard;

use crypto::scrypt::{scrypt_check, scrypt_simple, ScryptParams};

use mongodb::coll::{results, Collection};
use mongodb::db::{Database, ThreadedDatabase};

use bson::Bson::UtcDatetime;
use bson::{decode_document, encode_document, Bson, Document};

use super::super::oauth::Auth;

/* Returns a result whether a user provided the correct username and password, then returns a doc with
  user info. */
pub fn login(
    email: String,
    password: String,
    db: &Database,
) -> Result<Option<Document>, mongodb::Error> {
    let mut doc = Document::new();
    doc.insert("email", bson::Bson::String(email));
    /*attempt to find the user*/
    match db.collection("users").find_one(Some(doc), None) {
        Ok(res) => {
            match res {
                Some(doc) => {
                    /* check if password is correct. */
                    match scrypt_check(&password, doc.get("hash").unwrap().as_str().unwrap()) {
                        Ok(valid) => {
                            if valid {
                                Ok(Some(doc))
                            } else {
                                Err(mongodb::Error::DefaultError(
                                    "Incorrect Password or email".to_string(),
                                ))
                            }
                        } //TODO add logger.
                        Err(e) => Err(mongodb::Error::DefaultError(
                            "Scrypt hash error".to_string(),
                        )),
                    }
                }
                None => Err(mongodb::Error::DefaultError(
                    "Incorrect Password or email".to_string(),
                )),
            }
        }
        Err(e) => Err(mongodb::Error::DefaultError("Mongo Error".to_string())),
    }
}

/* creates a new user. */
pub fn create_user(
    email: String,
    password: String,
    user_type: i32,
    db: &Database,
) -> Result<results::InsertOneResult, mongodb::Error> {
    let mut doc = Document::new();
    doc.insert("email", bson::Bson::String(email));
    let _: Result<(), mongodb::Error> =
        match db.collection("users").find_one(Some(doc.clone()), None) {
            Ok(res) => match res {
                Some(doc) => {
                    return Err(mongodb::Error::DefaultError(
                        "email already taken".to_string(),
                    ))
                }
                None => Ok(()),
            },
            Err(e) => Ok(()),
        };
    let hash: String = scrypt_simple(&password, &ScryptParams::new(14, 8, 1)).expect("hash error");
    //doc.insert("email", bson::Bson::String(email));
    doc.insert("hash", bson::Bson::String(hash));
    doc.insert("user_type", bson::Bson::I32(user_type));
    db.collection("users").insert_one(doc, None)
}

/* Returns user info given the email */
pub fn user_info(email: String, db: &Database) -> Result<Option<Document>, mongodb::Error> {
    let mut doc = Document::new();
    doc.insert("email", bson::Bson::String(email));
    match db.collection("users").find_one(Some(doc), None) {
        Ok(res) => match res {
            Some(doc) => Ok(Some(doc)),
            None => Ok(None),
        },
        Err(e) => Err(mongodb::Error::DefaultError("mongo error".to_string())),
    }
}

/* updates user info, login credentials or user type */
pub fn update_user(
    email: String,
    new_email: Option<String>,
    new_password: Option<String>,
    new_user_type: Option<i32>,
    db: &Database,
) -> Result<(), mongodb::Error> {

    let mut search = doc!{"email": email};

    match search_doc(search.clone(), &db) {
        Ok(b) => {
            if !b {
                return Err(mongodb::Error::DefaultError("user not found".to_string()));
            }
        }
        Err(e) => return Err(e),
    }

    if new_email == None && new_password == None && new_user_type == None {
        return Err(mongodb::Error::DefaultError(
            "no update fields supplied".to_string(),
        ));
    }

    let mut update = Document::new();

    if let Some(em) = new_email {
        update.insert("email", bson::Bson::String(em));
    }

    if let Some(ps) = new_password {
        let hash: String = scrypt_simple(&ps, &ScryptParams::new(14, 8, 1)).expect("hash error");
        update.insert("hash", bson::Bson::String(hash));
    }

    if let Some(ut) = new_user_type {
        update.insert("user_type", bson::Bson::I32(ut));
    }

    match db.collection("users").update_one(search, doc!{"$set": update}, None) {
        Ok(res) => {
            println!("{:?}", res);
            Ok(())
        },
        Err(e) => {
            println!("{}", e);
            println!("update failed");
            Err(e)
        }
    }
}

/* Searches if a doc exists and returns a boolean result. */
fn search_doc(doc: Document, db: &Database) -> Result<bool, mongodb::Error> {
    match db.collection("users").find_one(Some(doc.clone()), None) {
        Ok(res) => match res {
            Some(doc) => Ok(true),
            None => Ok(false),
        },
        Err(t) => {
            println!("search failed");
            Err(t)
        }
    }
}

/* returns the user type, either manager, admin, or regular user */
pub fn get_user_type(email: String, db: &Database) -> Option<i32> {
    match db
        .collection("users")
        .find_one(Some(doc! {"email": email}), None)
    {
        Ok(res) => match res {
            Some(doc) => match doc.get_i32("user_type") {
                Ok(r) => Some(r),
                Err(e) => None,
            },
            None => None,
        },
        Err(e) => None,
    }
}

/* Given a user email, deletes the user */
pub fn delete_user(email: String, db: &Database) -> Result<(), mongodb::Error> {
    match db
        .collection("users")
        .delete_one(doc! {"email": email}, None)
    {
        Ok(res) => Ok(()),
        Err(e) => Err(e),
    }
}
