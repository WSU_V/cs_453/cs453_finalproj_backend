This is a backend written in rust by John Karasev

To run the backend run the commands below:

1) mongod --dbpath data/db --port 27017

2) curl https://sh.rustup.rs -sSf | sh

3) rustup default nightly

4) cargo run --bin seed --release # load in data into db

4) cargo run --bin app --release  # to start the backend server


The app uses port 8000, if there is something running on this port, kill it
or set a different port in Rocket.toml under production tab.



